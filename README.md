
# PAYCLIP ASSESMENT 

This project is meant for payclip assessment only started on 27/11/2019 and finished on 02/12/2019
- Author: Arturo Isaac Velazquez Vargas


## Project Overview

The project was created using the Microservices architectural pattern around the business functionalities (Transactions,Users) with Spring boot, Spring cloud and docker for developmnet and with kuberneetes on production.The lifecycle of the project is managed by maven and a mongodb NoSql database is used to persist the data.

The production environment is already up and running, it is deployed on AWS , It uses EKS (Amazon Elastic Kubernetes Service) to create a kubernetes cluster and to be able to orchestrate the microservices containers.This environment is already populated and it is ready to be tested

The project was structures using the kubernetes and spring cloud guidelines the following way

There are 3 infrastructure microservices

- Gateway-zuul (Spring-cloud provider)
- Discovery-server (Spring-cloud provider)
- Config server

There are 2 functional microservices

- Transactions
- Users

There is a mongodb database

1 instance for development and for production

Important note: There are another 2 repositories that contain the  kubernetes files for production deployment and another for the properties used for both environments (application.yml for spring cloud and configmaps for kubernetes)

This repositories are not needed to Run the application in development or to test it on production.Production is already up and running and ready for testing.
If you want to take a look at them to follow the production process you can clone them

KUBERNETES-PRODUCTION-REPOSITORY:https://gitlab.com/arturovelazquez94/arturo-velazquez-addition-production-deployment-test.git
CONFIG-SERVER-PROPERTIES:https://gitlab.com/arturovelazquez94/arturo-velazquez-addition-properties.git


This repository ONLY contains the JAVA code with MAVEN that is needed to compile and run in development (localhost)
There is no need to compile for production it is already up and running.


## Production environment

The project was deployed on AWS-EKS and here there are some important points 

- Region : us-west-2 (Oregon)
- Availabilities zones: us-west-2a, us-west-2b
- EC2 nodes: 3 nodes with t2.medium instance type
- There are 4 pods each one the the microservices (Zuul-Gateway,Transactions-microservice,Users-microservice,MongoDB Replicaset)
- There are 2 ELB (Load balancers) one for the Zuul-gateway and another one for Mongodb to be able to access them form the outside world
- EKS as a kubernetes master working with the 3 ec2 worker nodes

### Assesment test on production (IMPORTANT-NEEDED TO TEST IT ON PRODUCTION)

Important points to consider consume the rest endpoints

 - PROTOCOL:http
 - HOST:ad12ca7e913ff11eab56a0a906c8a208-2067301645.us-west-2.elb.amazonaws.com
 - PORT:7001
 - PREFIX:payclip

 E.g End point of Requirement 3- List all transations for the specific user
 
 GET
 >http://ad12ca7e913ff11eab56a0a906c8a208-2067301645.us-west-2.elb.amazonaws.com:7001/payclip/transactions/users/5de416d74d142f000132bb72
 
 E.g End point of Requirement 1- Create a transation for the specific user

 POST  
 >http://ad12ca7e913ff11eab56a0a906c8a208-2067301645.us-west-2.elb.amazonaws.com:7001/payclip/transactions/users/5de416d74d142f000132bb72

 Important note: In the project, it is a JSON Postman collection that must be imported to be able to have all the API enpoints, it is divided in two folders one for the User requests and another one for Transaction requests.Each one of the request has a example with a sample request and a sample response showing the api consumption.

Import Postman Collection link

- https://www.getpostman.com/collections/8ab018de0e9700b61aef

Import the collection as json file in the postman directory

- postman/Payclip.postman_collection.json

### MONGO DATABASE population

 The production database has already 1 user and 10 transactions related to the user 

### Compile and generate the images for production (NOT NEEDED) 

Important note: This step is not needed because the production environment is up and running , just to let you know how the project is compiled and the docker images are generated

To compile the code in production I used another maven profiled called 'production'

I compiled using the next command

>mvn clean install -DskipTests -P production

This command installs the jar in .m2 home and the most important is that I used the JIB (Java Image Builder) that is a google library that allow us to create the docker images and to upload them to a Docker hub or another cloud provider.So the images are created and uploaded to my docker hub account ready to use them in production with kubernetes.


### Assesment test on development (IMPORTANT)

The project (On development and production) is structured with maven pom parent and 4 modules it is called 'payclip-back-microservices' , each one for the respective microservice.

In addition there is another maven project that is called 'payclip-back-commons' that has a common classes and methods used by the 2 microservices

Note: If you enter the 'payclip-back microservices' there is a .env that contains certain variables that I used to configure the environment and to speed up the development process in a dynamic way.If you want to access and take a look at it just type the next command

>nano payclip-back-microservices/.env


### Steps to compile and set up in DEVELOPMENT (IMPORTANT-NEEDED TO SET UP IN DEVELOPMENT)

The microservices are deployed into docker containers so I used docker-compose to speed up the development

Run the next command to run compile the code and to set up the containers in one line.First enter the directory 'payclip-back-main'

>cd payclip-back-main/
 
 and execute 

>cd payclip-back-commons/ && mvn clean install && cd ../payclip-back-microservices/ && mvn clean install -DskipTests -P development && docker-compose up --build

Note: I used two maven profiles one for production and another one for development because of the dependencies needed for each one of the environments.
Note: Once the environment is up and running you should WAIT for about 3 minutes(It depends on the hardware)so the ZUUL-GATEWAY can register each one of the microservices successfully.Sometimes If ZUUL is not given enough time it keeps throwing an exception till it registers the microservices.Once you have waited the 3 minutes that ZUUL needs to work, you can start using the REST web services and start testing them with POSTMAN or with a Web client


### POSTMAN collection (IMPORTANT)

As I mentioned above, there is a collection JSON to be imported to postman.I created 2 working environments one for DEVELOPMENT and another for PRODUCTION and the values for the environments are the following

For production

- host_gateway=ad12ca7e913ff11eab56a0a906c8a208-2067301645.us-west-2.elb.amazonaws.com
- port_gateway=7001 

For development

- host_gateway=localhost
- port_gateway=7001

You need to add them to the environments 

Note: All the endpoints are configures to the environment variables so they are dynamic and not static.

E.g Static api consumption

>http://ad12ca7e913ff11eab56a0a906c8a208-2067301645.us-west-2.elb.amazonaws.com:7001/payclip/transactions/users/5de416d74d142f000132bb72

E.g Dinamic api consumption with environment variables

>http://{{host_gateway}}:{{port_gateway}}/payclip/transactions/users/5de416d74d142f000132bb72

### MONGODB

Production
It is exposed by a AWS load balancer and you can access it with the next host and port

HOST:aa090449e139611eab56a0a906c8a208-1054488111.us-west-2.elb.amazonaws.com
PORT:27020
DATABASE:payclip


Establish conection with the mongodb server

>mongo --host aa090449e139611eab56a0a906c8a208-1054488111.us-west-2.elb.amazonaws.com --port 27020

Or with a free MongoDB GUI like Robo3t (I use it) or Robomongo

One the conection is stablished there is a dabase called payclip where all the information is persisted

>kubectl create clusterrolebinding admin --clusterrole=cluster-admin --serviceaccount=default:default

Development
HOST:localhost
PORT:27020
DATABASE:payclip

### IMPORTAN POINTS IN THE ASSESMENT

- I used a NoSql database mainly for the data modeling (JSON), the scalability (using replicasets) and flexibility (Schemeless database) 
- As the resource identifiers (Ids) for the Transaction and User resources I used the id generator that mongodb gives us.The 'ObjectId' which in summary is a

The 12-byte value that consists of:

- 4-byte value representing the seconds since the Unix epoch,
- 5-byte random value, and
- 3-byte counter, starting with a random value.

Which garantees the unicity value if the id

### NEXT STEPS IN A REAL PRODUCT DEVELOPMENT 

- Add a secutity layer with Spring security and some pattern like RBAC with OAuth 2.0 or ConnectId
- Scale the mongodb replicaset from 1 or 3 to 5 or seven to high availability
- Add jennkins to manage the IC/DI
- Correct work of gitflow
- SonarQube
- Versionate the api web services
- Maybe sacle the AWS nodes from 3 to 5 depending on the demand
- Use Contract testing in microservices
- Add AOP to register traversal functionalities


### Project feedback

This assesment project was really a challange for me, specially for the deadline, but at the end I could finish it on time and I practiced my programming and architectural skills.During the development I faced some troubles related with the requirement 6 and on production with the kubernetes cluster integrated with spring cloud but I the end I had fun and enjoy the development.

This was a really outstanding assesment because it evaluates your problem solving skills as well as your programming skills.

## OBSERVATIONS

In case you have any doubt about the project structure, microservice infrastructure ,AWS architecture, or Api REST consumption.You can contact me

- Email:arturovelazquez94@gmail.com
- LinkedIn:https://www.linkedin.com/in/arturo-isaac-velazquez-vargas-a93742bb
- Phone:5511451444 


## License
[MIT](https://choosealicense.com/licenses/mit/)
