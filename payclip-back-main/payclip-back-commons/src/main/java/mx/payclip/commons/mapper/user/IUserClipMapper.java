package mx.payclip.commons.mapper.user;

import org.mapstruct.Mapper;

import mx.payclip.commons.mapper.utils.GenericMapper;
import mx.payclip.commons.mapper.utils.StringObjectIdMapper;
import mx.payclip.commons.payload.request.user.UserClipRequest;
import mx.payclip.commons.payload.response.user.UserClipResponse;
import mx.payclip.commons.persistence.collection.UserClip;

/**
 * This interface helps to map the DTO User resource(Entity,Request,Response)
 * 
 * @author Arturo Isaac Velázquez Vargas
 *
 */
@Mapper(componentModel = "spring", uses = { StringObjectIdMapper.class })
public interface IUserClipMapper extends GenericMapper<UserClip, UserClipRequest, UserClipResponse> {

	public UserClipRequest entityToRequest(UserClip userClip);
}
