package mx.payclip.commons.mapper.transaction;

import org.mapstruct.Mapper;

import mx.payclip.commons.mapper.utils.GenericMapper;
import mx.payclip.commons.mapper.utils.StringObjectIdMapper;
import mx.payclip.commons.payload.request.transaction.clip.crud.TransactionClipRequest;
import mx.payclip.commons.payload.response.transaction.TransactionClipResponse;
import mx.payclip.commons.persistence.collection.TransactionClip;

/**
 * This interface helps to map the DTO Transaction
 * resource(Entity,Request,Response)
 * 
 * @author Arturo Isaac Velázquez Vargas
 *
 */
@Mapper(componentModel = "spring", uses = { StringObjectIdMapper.class })
public interface ITransactionClipMapper
		extends GenericMapper<TransactionClip, TransactionClipRequest, TransactionClipResponse> {
}
