package mx.payclip.users.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import mx.payclip.commons.persistence.collection.Privilege;

@Repository
public interface IUserPrivilegeRepository extends MongoRepository<Privilege, String> {

}
