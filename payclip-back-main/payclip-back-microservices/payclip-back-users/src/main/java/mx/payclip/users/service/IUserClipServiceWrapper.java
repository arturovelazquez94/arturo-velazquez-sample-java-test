package mx.payclip.users.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import mx.payclip.commons.payload.request.user.UserClipRequest;
import mx.payclip.commons.payload.response.user.UserClipResponse;
import mx.payclip.commons.persistence.collection.Privilege;
import mx.payclip.commons.persistence.collection.Role;

/**
 * This interface contains the methods related to CRUD operations
 * 
 * @author Arturo Isaac Velazqeuz Vargas
 *
 */
public interface IUserClipServiceWrapper {

	UserClipResponse findById(String id);

	UserClipResponse findByUseName(String userName);

	Page<UserClipResponse> findAll(Pageable pageable);

	UserClipResponse create(UserClipRequest transaction);

	UserClipResponse update(UserClipRequest transaction, String id);

	void delete(String id);
	// ----------------------RBAC---------------------------//

	Role create(Role role);

	Privilege create(Privilege privilege);

	void assignPrivilege(String roleId, String privilegeId);

	void assignRole(String userId, String roleId);

}
